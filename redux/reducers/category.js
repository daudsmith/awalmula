import { FETCH_CATEGORY_BEGIN, FETCH_CATEGORY_SUCCESS, FETCH_CATEGORY_FAILURE, FETCH_PRODUCT_BEGIN, FETCH_PRODUCT_SUCCESS, FETCH_PRODUCT_FAILURE } from '../actions/category'

const initialCategory = {
	isFetching: false,
	isError: false,
	result: {
		data: null,
  		isFetching: false, 
  		error: false
	},
	product: {
		data: null,
  		isFetching: false, 
  		error: false
	}
}

const categoryReducer = ( state = initialCategory , action ) => {
  	switch( action.type ){
  		
		
		case 'FETCH_CATEGORY_BEGIN':
				state = {...state, 
					result: {
						...state.category,
						isFetching: true, 
						error: false
				   }
				}
				break;
		case 'FETCH_CATEGORY_SUCCESS':
				state = { ...state, 
					  result: {
						  ...state.category,
						  data: action.payload,
						  isFetching: false, 
						  error: false
					 }
				  }
				break;
		case 'FETCH_CATEGORY_FAILURE':
				state = {...state, 
					result: {
						...state.category,
						data: action.payload,
						isFetching: false, 
						error: true
				   }
				}
				break;
			case 'FETCH_PRODUCT_BEGIN':
					state = {...state, 
						product: {
							...state.product,
							isFetching: true, 
							error: false
					   }
					}
					break;
			case 'FETCH_PRODUCT_SUCCESS':
					state = { ...state, 
						  product: {
							  ...state.product,
							  data: action.payload,
							  isFetching: false, 
							  error: false
						 }
					  }
					break;
			case 'FETCH_PRODUCT_FAILURE':
					state = {...state, 
						product: {
							...state.product,
							data: action.payload,
							isFetching: false, 
							error: true
					   }
					}
					break;
	    default:
	      	break;
	}
  	return state
}

export default categoryReducer;

import { FETCH_PRODUCT_BEGIN, FETCH_PRODUCT_SUCCESS, FETCH_PRODUCT_FAILURE } from '../actions/product'

const initialProduct = {
	isFetching: false,
	isError: false,
	product: {
		data: null,
  		isFetching: false, 
  		error: false
	}
}

const productReducer = ( state = initialProduct , action ) => {
  	switch( action.type ){
  		
			case 'FETCH_PRODUCT_BEGIN':
					state = {...state, 
						product: {
							...state.product,
							isFetching: true, 
							error: false
					   }
					}
					break;
			case 'FETCH_PRODUCT_SUCCESS':
					state = { ...state, 
						  product: {
							  ...state.product,
							  data: action.payload,
							  isFetching: false, 
							  error: false
						 }
					  }
					break;
			case 'FETCH_PRODUCT_FAILURE':
					state = {...state, 
						product: {
							...state.product,
							data: action.payload,
							isFetching: false, 
							error: true
					   }
					}
					break;
	    default:
	      	break;
	}
  	return state
}

export default productReducer;

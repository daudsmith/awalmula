import { GET_CATEGORY_PRODUCT } from '../../api'
import httpRequest from '../../api/services'

export const FETCH_PRODUCT_BEGIN = 'FETCH_PRODUCT_BEGIN';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';


export const fetchProductBegin = () => ({
	type: 'FETCH_PRODUCT_BEGIN'
});

export const fetchProductSuccess = data => ({
	type: 'FETCH_PRODUCT_SUCCESS',
	payload: data
});

export const fetchProductFailure = error => ({
	type: 'FETCH_PRODUCT_FAILURE',
	payload: error
});

export const requestProductGet = () => {
	return dispatch => {
		dispatch(fetchProductBegin())
		
	  	return httpRequest.get(GET_CATEGORY_PRODUCT).then(function (response) {
	     	if (response.data.code == 200) {
	     		dispatch(fetchProductSuccess(response.data.data))
		      	return response.data.data;
	     	} else {
	     		dispatch(fetchProductFailure(response.data))
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	  		
	     	dispatch(fetchProductFailure(error.response))
	  	});
	}
}
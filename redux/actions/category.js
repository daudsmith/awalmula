import { GET_CATEGORY, GET_CATEGORY_PRODUCT } from '../../api'
import httpRequest from '../../api/services'

export const FETCH_CATEGORY_BEGIN = 'FETCH_CATEGORY_BEGIN';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';
export const FETCH_CATEGORY_FAILURE = 'FETCH_CATEGORY_FAILURE';

export const FETCH_PRODUCT_BEGIN = 'FETCH_PRODUCT_BEGIN';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';


export const fetchCategoryBegin = () => ({
	type: 'FETCH_CATEGORY_BEGIN'
});

export const fetchCategorySuccess = data => ({
	type: 'FETCH_CATEGORY_SUCCESS',
	payload: data
});

export const fetchCategoryFailure = error => ({
	type: 'FETCH_CATEGORY_FAILURE',
	payload: error
});


export const fetchProductBegin = () => ({
	type: 'FETCH_PRODUCT_BEGIN'
});

export const fetchProductSuccess = data => ({
	type: 'FETCH_PRODUCT_SUCCESS',
	payload: data
});

export const fetchProductFailure = error => ({
	type: 'FETCH_PRODUCT_FAILURE',
	payload: error
});

export const requestCategoryGet = () => {
	return dispatch => {
		dispatch(fetchCategoryBegin())
		
	  	return httpRequest.get(GET_CATEGORY).then(function (response) {
	     	if (response.data.code == 200) {
	     		dispatch(fetchCategorySuccess(response.data.data))
		      	return response.data.data;
	     	} else {
	     		dispatch(fetchCategoryFailure(response.data))
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	  		
	     	dispatch(fetchCategoryFailure(error.response))
	  	});
	}
}

export const requestProductGet = () => {
	return dispatch => {
		dispatch(fetchProductBegin())
		
	  	return httpRequest.get(GET_CATEGORY_PRODUCT).then(function (response) {
			  console.log("ini response", response)
	     	if (response.data.code == 200) {
	     		dispatch(fetchProductSuccess(response.data.data))
		      	return response.data.data;
	     	} else {
	     		dispatch(fetchProductFailure(response.data))
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	  		
	     	dispatch(fetchProductFailure(error.response))
	  	});
	}
}
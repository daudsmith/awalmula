export const GET_CATEGORY = `https://staging-cuan.awalmula.co/rest/default/V1/categories`;
export const GET_PRODUCT = `${process.env.API_HOST}/categories/{categoryId}/products`;
export const GET_CATEGORY_PRODUCT = `https://staging-cuan.awalmula.co/rest/default/V1/products?searchCriteria[pageSize]=10`;
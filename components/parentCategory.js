import React from 'react';
import { Col, Row, Button } from 'reactstrap'
import { connect } from 'react-redux'
import { requestCategoryGet } from '../redux/actions/category'

class Category extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
   }
   
   render() {
      
      let data = this.props.category.result.data;

      return (
         <div className="d-flex justify-content-center p-5">
            <Row>
                { data && data.children_data.map ((category, i) => {
                    return <div key={i}>
                        { category && category.children_data.map ((food, j) => {
                            return <Col xs="12" className="content-category" key={j}>
                                <Button
                                outline block
                                className='object-category'
                                color="success">{food.name}</Button>
                            </Col>
                        })}
                    </div>
                }
                )}
            </Row>
            <style jsx global>
               {`
                  .content-category {
                     margin-bottom: 5px;
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                     justify-content: center;
                  }
                  .object-category {
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                  }
                  .logo {
                     width: 35px;
                     height: 35px;
                     justify-content: center;
                     margin: 0 auto;
                     padding: 0 auto;
                   }
                  .card-category{
                     display:flex;
                     box-shadow: 3px 2px 10px rgba(189, 195, 199,1.0); 
                     padding: 2px;
                     margin-bottom: 6px;
                  }
               `}
            </style>
         </div>
      );
   }
}

const mapStateToProps = (state) => {
   return {
      category: state.category
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestCategoryGet: () => dispatch(requestCategoryGet()),
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)
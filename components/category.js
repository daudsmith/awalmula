import React from 'react';
import Image from 'next/image'
import { Container, Col, Row, Card, CardBody, TabContent, TabPane, Button, Input, Nav, NavItem, NavLink, CardTitle, CardText } from 'reactstrap'
import logo from '../assets/logo.jpg'
import { connect } from 'react-redux'
import { requestCategoryGet } from '../redux/actions/category'
import ParentCategory from './parentCategory';
import ChildrenCategory from './childrenCategory';

class Category extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
   }
   
   render() {
      
      let data = this.props.category.result.data;

      return (
         <div className="d-flex justify-content-center p-5">
            <Container>
                  <Card className='card-category'>
                     <CardBody>
                        <Row>
                              <Col xs={1}>
                                 <div className='logo'>
                                  <Image src={logo}/>
                                 </div>
                              </Col>
                              <Col xs={1}>
                              <Button
                                    outline block
                                    className='object-category'
                                    color="success">Category</Button>
                              </Col>
                              <Col xs={8}>
                                    <Input className='object-category' placeholder='search'/>
                              </Col>
                              <Col xs='1'>
                                 <div className='content-category'>
                                 <Button
                                    outline block
                                    className='object-category'
                                    color="success">Login</Button>
                                 </div>
                              </Col>
                              <Col xs='1'>
                                 <div className='content-category'>
                                 <Button
                                    outline block
                                    className='object-category'
                                    color="success">Register</Button>
                                 </div>
                              </Col>
                        </Row>
                     </CardBody>
                  </Card>
                  <Card className='card-category'>
                           <CardBody>
                              <Row>
                                <Col xs="6">
                                    <ParentCategory/>
                                </Col>
                                <Col xs="6">
                                    <ChildrenCategory/>
                                </Col>
                              </Row>
                           </CardBody>
                        </Card>
            </Container>
            <style jsx global>
               {`
                  .content-category {
                     margin-bottom: 5px;
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                     justify-content: center;
                  }
                  .object-category {
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                  }
                  .logo {
                     width: 35px;
                     height: 35px;
                     justify-content: center;
                     margin: 0 auto;
                     padding: 0 auto;
                   }
                  .card-category{
                     display:flex;
                     box-shadow: 3px 2px 10px rgba(189, 195, 199,1.0); 
                     padding: 2px;
                     margin-bottom: 6px;
                  }
               `}
            </style>
         </div>
      );
   }
}

const mapStateToProps = (state) => {
   return {
      category: state.category
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestCategoryGet: () => dispatch(requestCategoryGet()),
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(Category)
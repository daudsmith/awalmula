import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
  {
    src: './assets/1.jpg',
    key: '1'
  },
  {
    src: './assets/3.jpg',
    key: '3'
  }
];

const Carousel = () => 
    <div className="carousel-style">
        <UncontrolledCarousel items={items} />
    <style jsx global>
        {`
        .carousel-style{
            width: 100%;
            height: 27%;
            margin-top: 20px;
            box-shadow: 1px 1px 6px #636e72;
        }
        `}
    </style>
</div>

export default Carousel;

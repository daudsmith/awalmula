import React from 'react';
import Image from 'next/image'
import Carousel from './carousel'
import { Container, Col, Row, Card, CardBody, TabContent, TabPane, Button, Input, CardImg } from 'reactstrap'
import logo from '../assets/logo.jpg'
import { connect } from 'react-redux'
import { withRouter } from 'next/router';
import { requestCategoryGet } from '../redux/actions/category'
import { requestProductGet } from '../redux/actions/product'

class HomePage extends React.Component {
   constructor(props) {
      super(props);
      console.log(props)
      this.state = {
     };
     this.moreCategory = this.moreCategory.bind(this);
   }
   

   toggle(tab) {
      if (this.state.activeTab !== tab) {
         this.setState({
            activeTab: tab
         });
      } else {
         this.setState({
            activeTab: null
         });
      }
   }   

   componentDidMount() {
      this.props.requestCategoryGet();
      this.props.requestProductGet();
   }

   moreCategory() {
      this.props.router.push({
         pathname: '/category'
      })
   }



   render() {
      
      let data = this.props.category.result.data;
      let product = this.props.product.product.data;

      return (
         <div className="d-flex justify-content-center p-5">
            <Container>
                  <Card className='card-category'>
                     <CardBody>
                        <Row>
                              <Col xs={1}>
                                 <div className='logo'>
                                  <Image src={logo}/>
                                 </div>
                              </Col>
                              <Col xs={1}>
                              <Button
                                    onClick={() => { this.toggle('1'); }} outline block
                                    className='object-category'
                                    color="success">Category</Button>
                              </Col>
                              <Col xs={8}>
                                    <Input className='object-category' placeholder='search'/>
                              </Col>
                              <Col xs='1'>
                                 <div className='content-category'>
                                 <Button
                                    outline block
                                    className='object-category'
                                    color="success">Login</Button>
                                 </div>
                              </Col>
                              <Col xs='1'>
                                 <div className='content-category'>
                                 <Button
                                    outline block
                                    className='object-category'
                                    color="success">Register</Button>
                                 </div>
                              </Col>
                        </Row>
                     </CardBody>
                  </Card>
                  <TabContent activeTab={this.state.activeTab}>
                     <TabPane tabId="1">
                        <Card className='card-category'>
                           <CardBody>
                              <Row>
                                    { data && data.children_data.map ((category, i) => {
                                 return <Col xs="4" key={i} className="content-category">
                                       <Button
                                          onClick={this.moreCategory} outline block
                                          className='object-category'
                                          color="success">{category.name}</Button>
                                    </Col>
                                 }
                                 )} 
                              </Row>
                           </CardBody>
                        </Card>
                     </TabPane>
                  </TabContent>
                  <Carousel/>
                  <Card className='product-category'>
                     <CardBody>
                     <Row>
                        { product && product.items.map ((item, i) => {
                           console.log(item)
                        return <Col xs="4" key={i} className="content-category">
                              <Card>
                              <CardImg
                                 alt="Card image cap"
                                 src="https://picsum.photos/318/180"
                                 top
                                 width="100%"
                              />
                                 <CardBody>
                                    <Col xs='12'>
                                       {item.name}
                                    </Col>
                                    <Col xs='12'>
                                      Rp {item.price}
                                    </Col>
                                 </CardBody>
                              </Card>
                           </Col>
                        }
                        )} 
                     </Row>
                     </CardBody>
                  </Card>
            </Container>
            <style jsx global>
               {`
                  .product-category {
                     margin-top: 25px;
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                     justify-content: center;
                  }
                  .content-category {
                     margin-bottom: 5px;
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                     justify-content: center;
                  }
                  .object-category {
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                  }
                  .logo {
                     width: 35px;
                     height: 35px;
                     justify-content: center;
                     margin: 0 auto;
                     padding: 0 auto;
                   }
                  .card-category{
                     display:flex;
                     box-shadow: 3px 2px 10px rgba(189, 195, 199,1.0); 
                     padding: 2px;
                     margin-bottom: 6px;
                  }
               `}
            </style>
         </div>
      );
   }
}

const mapStateToProps = (state) => {
   return {
      category: state.category,
      product: state.product,
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestCategoryGet: () => dispatch(requestCategoryGet()),
      requestProductGet: () => dispatch(requestProductGet()),
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(HomePage))
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import withRedux from 'next-redux-wrapper'
import initStore from '../redux/store'
import { Provider, connect } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

function MyApp({ Component, pageProps, store }) {
    
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={store.__persistor}>
            <Component {...pageProps} />
        </PersistGate>
      </Provider>
    );
}

export default withRedux(initStore, { debug: false })(MyApp)

